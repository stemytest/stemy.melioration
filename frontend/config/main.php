<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
	'defaultRoute' => 'site/index',
    'components' => [
		'db' => [
			'class' => 'yii\db\Connection',
			'dsn' => 'mysql:host=localhost;dbname=u0888185_melioration',
			'username' => 'u0888185_stemy',
			'password' => 'SeReGa280806',
			'charset' => 'utf8',
		],
        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
		'urlManager' => [
            'enablePrettyUrl' => true,
            //'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => [
                //'dashboard/' => 'dashboard/default/index', //модуль статистики
                '' => 'site/index',
                '<action>'=>'site/<action>',
                //['class' => 'yii\rest\UrlRule', 'controller' => 'rest'],
                //['class' => 'yii\rest\UrlRule', 'controller' => 'rest', 'pluralize' => false],
				//['class' => 'yii\rest\UrlRule', 'controller' => '/rest/process-opirations/create'],
            ],
        ],
		'assetManager' => [
            'basePath' => '@webroot/assets',
            'baseUrl' => '@web/assets',
			'bundles' => [
				'kartik\form\ActiveFormAsset' => [
                'bsDependencyEnabled' => false // do not load bootstrap assets for a specific asset bundle
				],
			],
        ],  
        'request' => [
            'baseUrl' => ''
        ],
        /*
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        */
    ],
    'params' => $params,
	
	'modules' => [ 
        'gii' => [ 
            'class' => 'yii\gii\Module', 
            'allowedIPs' => ['*'] 
        ], 
        'debug' => [ 
            'class' => 'yii\debug\Module', 
            'allowedIPs' => ['*'] 
        ],
		'gridview' =>  [
			'class' => '\kartik\grid\Module'
		],
		'dashboard' => [
            'class' => 'app\modules\dashboard\Dashboard',
            'layout' => '@app/views/layouts/adminlte/layouts/main',
        ],
		'directory' => [
            'class' => 'app\modules\directory\Directory',
            'layout' => '@app/views/layouts/adminlte/layouts/main',
        ],
        'map' => [
            'class' => 'app\modules\map\Map',
            'layout' => '@app/views/layouts/adminlte/layouts/main',
        ],
    ],
];
